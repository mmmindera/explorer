package com.minderaschool.explorer.agent;

public class Square {
    private boolean top;
    private boolean bottom;
    private boolean right;
    private boolean left;

    private Square topNeighbor;
    private Square bottomNeighbor;
    private Square rightNeighbor;
    private Square leftNeighbor;


    public Square(Square topNeighbor, Square bottomNeighbor, Square rightNeighbor, Square leftNeighbor) {
        this.topNeighbor = topNeighbor;
        this.bottomNeighbor = bottomNeighbor;
        this.rightNeighbor = rightNeighbor;
        this.leftNeighbor = leftNeighbor;
    }

    public Square() {

    }

    public void setTop(boolean top) {
        this.top = top;
    }

    public void setBottom(boolean bottom) {
        this.bottom = bottom;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public boolean isTop() {
        return top;
    }

    public boolean isBottom() {
        return bottom;
    }

    public boolean isRight() {
        return right;
    }

    public boolean isLeft() {
        return left;
    }

    public Square getTopNeighbor() {
        return topNeighbor;
    }

    public void setTopNeighbor(Square topNeighbor) {
        this.topNeighbor = topNeighbor;
    }

    public Square getBottomNeighbor() {
        return bottomNeighbor;
    }

    public void setBottomNeighbor(Square bottomNeighbor) {
        this.bottomNeighbor = bottomNeighbor;
    }

    public Square getRightNeighbor() {
        return rightNeighbor;
    }

    public void setRightNeighbor(Square rightNeighbor) {
        this.rightNeighbor = rightNeighbor;
    }

    public Square getLeftNeighbor() {
        return leftNeighbor;
    }

    public void setLeftNeighbor(Square leftNeighbor) {
        this.leftNeighbor = leftNeighbor;
    }
}
