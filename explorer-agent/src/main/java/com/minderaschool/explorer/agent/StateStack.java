package com.minderaschool.explorer.agent;

public class StateStack {

    private Brain.State currentState;
    private char turning;

    public StateStack(Brain.State state) {
        this.currentState = state;
    }

    public StateStack(Brain.State state,char turning){
        this.currentState = state;
        if(turning=='r') this.turning='l';
        if(turning=='l') this.turning='r';
    }
    public StateStack() { }


    public Brain.State getCurrentState() {
        return currentState;
    }

    public char getTurning() {
        return turning;
    }
}
